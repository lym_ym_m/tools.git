export function getLastNumberDays (num, separator){
  const days = [];
  for (let i = 0; i < num; i++) {
    const dateStr = new Date(
      new Date().setDate(
        new Date().getDate() - i
      )
    ).toLocaleDateString().toString().slice(5)
    const dateList = dateStr.split("/");
    for(let j = 0; j < dateList.length; j++) {
      dateList[j] = dateList[j].length < 2 ? '0'+dateList[j] : dateList[j];
    }
    days.unshift(dateList.join(separator || "/" ));
  }
  return days;
}